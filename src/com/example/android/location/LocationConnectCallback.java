package com.example.android.location;

import android.app.Activity;
import android.util.Log;
import com.koushikdutta.async.http.socketio.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 8/7/13
 * Time: 6:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class LocationConnectCallback implements ConnectCallback, DisconnectCallback,
        ErrorCallback, JSONCallback, StringCallback, EventCallback {

    Activity activity;
    String TAG = "LocationConnectCallback";
    public LocationConnectCallback(Activity activity) {
           this.activity = activity;
    }
    @Override
    public void onConnectCompleted(Exception ex, SocketIOClient client) {
        Log.d(TAG, "onConnectCompleted");
        if (ex != null) {
            Log.d(TAG, "ex==null" + ex);
            return;
        }

        //Save the returned SocketIOClient instance into a variable so you can disconnect it later
        client.setDisconnectCallback(this);
        client.setErrorCallback(this);
        client.setJSONCallback(this);
        client.setStringCallback(this);

        client.addListener("map_event", LocationConnectCallback.this);

        //You need to explicitly specify which events you are interested in receiving

        client.of("/", new ConnectCallback() {

            @Override
            public void onConnectCompleted(Exception ex, SocketIOClient client) {
                Log.d(TAG, "onConnectCompleted2");

                if (ex != null) {
                    Log.d(TAG, "ex2==null");
                    ex.printStackTrace();
                    return;
                }
                JSONObject jsonObject = new JSONObject();
                try {
                    Log.d(TAG, "sending object");

                    jsonObject.put("lat", 32.24);
                    client.emit(jsonObject);


                } catch (JSONException e) {
                    Log.d(TAG, "exception " + e);

                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                //This client instance will be using the same websocket as the original client,
                //but will point to the indicated endpoint
                client.setDisconnectCallback(LocationConnectCallback.this);
                client.setErrorCallback(LocationConnectCallback.this);
                client.setJSONCallback(LocationConnectCallback.this);
                client.setStringCallback(LocationConnectCallback.this);
                client.addListener("a message", LocationConnectCallback.this);

            }
        });
    }




    @Override
    public void onDisconnect(Exception e) {
        Log.d(TAG, "onDisconnect");
    }

    @Override
    public void onError(String error) {
        Log.d(TAG, "onError");

    }

    @Override
    public void onJSON(JSONObject json, Acknowledge acknowledge) {
        Log.d(TAG, "onJSON");
    }

    @Override
    public void onString(String string, Acknowledge acknowledge) {
        Log.d(TAG, "onString");
    }

    @Override
    public void onEvent(String event, JSONArray argument, Acknowledge acknowledge) {
        Log.d(TAG, "onEvent event: " + event + " argument: " + argument.toString());
    }
}
